class UsersController < ApplicationController
  def new
    if current_user
      redirect_to friends_path
    else
        @user = User.new
    end
  end

  def create
     @user = User.new(params[:user])
      if @user.save
        session[:user_id] = @user.id
        UserNotifier.welcome_email(@user).deliver_now
        redirect_to @user , notice: "Thank you for signing up"
      else
          render 'new'
      end
  end

  def index
    @users = User.all
    @twitz = Twitz.new
  end

  def friends
    if current_user
      @twitz = Twitz.new
      friends_ids = current_user.followeds.map(&:id).push(current_user.id)
      @twitzs = Twitz.where(user_id: friends_ids)
    else
      redirect_to root_url
    end

  end

  def show
    @user = User.find_by_id(params[:id])
    @twitz = Twitz.new
    @relationship =  Relationship.where(
      follower_id: current_user,
      followed_id: @user.id
      ).first_or_initialize if current_user
  end

  def edit
    @user = User.find(params[:id])
    redirect_to @user unless @user == current_user
  end
  def update
     @user = User.find(params[:id])
     if @user.update_attributes(params[:user])
       redirect_to @user, notice: "Profile Updated!!"
     else
       render 'edit'
     end
  end
end
