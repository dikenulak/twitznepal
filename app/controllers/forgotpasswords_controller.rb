class ForgotpasswordsController < ApplicationController
  def create
    user = User.find_by(email: params[:email].downcase)
    if user
      @userp = user.password
      UserNotifier.reset_email(user).deliver_now
      redirect_to forgotpasswords_path, notice: "password Recovered!!."
    else
        flash[:error] = "Wrong Email id."
        redirect_to forgotpasswords_path
    end
  end
end
