class UserNotifier < ApplicationMailer
  def welcome_email(user)
    @user = user
    mail( :to => @user.email,
    :subject => 'Thanks for signing up for our amazing Website' )
  end
  def reset_email(user)
    @user = user
    mail( :to => @user.email,
    :subject => 'Your password has been recovered here')
  end
end
