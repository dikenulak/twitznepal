class ApplicationMailer < ActionMailer::Base
  default from: "diken5nator@gmail.com"
  layout 'mailer'
end
